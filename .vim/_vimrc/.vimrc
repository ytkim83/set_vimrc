"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 
" - Subj : Yeogi Vim Configurations
" - Auth : kimty83@with.co.kr
" - Rele : 2017.01.05 ( The )
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"set guifont=Liberation\ Mono\ for\ Powerline\ 10

"https://github.com/moll/vim-node#installing
set runtimepath^=~/.vim/bundle/node

" -------------------------------------------------------------------------------------------
" apply colorscheme
" -------------------------------------------------------------------------------------------
set t_Co=256
syntax on
syntax enable
"colorscheme tender
colorscheme molokai2
"colorscheme mango
"colorscheme sweyla988775
"colorscheme solarized

"au BufReadPost,BufNewFile *.twig colorscheme koehler 
"au BufReadPost,BufNewFile *.css colorscheme slate
"au BufReadPost,BufNewFile *.js colorscheme slate2
"au BufReadPost,BufNewFile *.js colorscheme monokai
"au BufReadPost,BufNewFile *.js colorscheme wombat
"au BufReadPost,BufNewFile *.py colorscheme molokaiyo
"au BufReadPost,BufNewFile *.html colorscheme monokai
"au BufReadPost,BufNewFile *.java colorscheme monokai
"au BufReadPost,BufNewFile *.php colorscheme monokai

" -------------------------------------------------------------------------------------------
" Cursor
" -------------------------------------------------------------------------------------------
set cursorline
" Default line highlighting for unknown filetypes
hi CursorLine ctermbg=233
hi CursorLine guibg=#D3D3D3 cterm=none
au FileType php set cursorline!
"hi String ctermfg=140
"hi Visual guibg=white guifg=black gui=NONE ctermfg=black ctermbg=white cterm=reverse
"hi Cursor guibg=white guifg=black gui=NONE ctermfg=black ctermbg=white cterm=reverse
"hi iCursor guibg=white guifg=black gui=NONE ctermfg=black ctermbg=white cterm=reverse

set nu
set encoding=utf-8
set fileencodings=utf8,euc-kr   " vi에서 파일 인코딩 자동 확인
set smartindent                 " 코딩 작업시 지능적인 들여쓰기
set autoindent                  " 코딩 작업시 자동적용 들여쓰기
set hlsearch                    " 일반모드에서 / 검색시 하이라이팅
set ignorecase                  " 일반모드에서 / 검색시 대소문자 구분하지 않음
set showmatch                   " 닫는 괄호를 입력할 때 일치하는 괄호 보여줌
set softtabstop=4
set tabstop=4
set shiftwidth=4
"set expandtab                   " tab을 space로 인식

au FileType * setl fo-=cro      " 자동 주석 처리 off

" -------------------------------------------------------------------------------------------
" Function Key
" -------------------------------------------------------------------------------------------
" 폴딩 기능 키 매핑
map <F5> v]}zf
" NERD Tree는 F2키. Tag List는 F3키에 매칭.
nmap <F2> :NERDTree<CR>
nmap <F3> :TlistToggle<CR>

" -------------------------------------------------------------------------------------------
" Key Setting - resize windows
" -------------------------------------------------------------------------------------------
nnoremap <silent> <Leader>= :exe "resize +3"<CR>
nnoremap <silent> <Leader>- :exe "resize -3"<CR>
nnoremap <silent> <Leader>] :exe "vertical resize +8"<CR>
nnoremap <silent> <Leader>[ :exe "vertical resize -8"<CR>
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>_ :exe "resize " . (winheight(0) * 2/3)<CR>
nnoremap <silent> <Leader>} :exe "vertical resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>{ :exe "vertical resize " . (winheight(0) * 2/3)<CR>

" -------------------------------------------------------------------------------------------
" Set Buffer 
" -------------------------------------------------------------------------------------------
" 버퍼를 수정한 직후 버퍼를 숨김
set hidden
" 버퍼 새로 열기
nmap <leader>n :enew<cr>
" 현재 버퍼를 닫고 이전 버퍼로 이동
nmap <leader>q :bp <BAR> bd #<CR>
" 다음 버퍼로 이동
nmap <C-l> :bnext<CR>
" 이전 버퍼로 이동
nmap <C-h> :bprevious<CR>
" 모든 버퍼와 각 버퍼 상태 출력
"nmap <leader>bl :ls<CR>

"--------------------------------------------------------------------------------------------------
" Set Vundle Plugin List for apply
"--------------------------------------------------------------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'

" search files
Plugin 'ctrlpvim/ctrlp.vim'

" multiple cursors
Plugin 'terryma/vim-multiple-cursors'

" plugin from http://vim-scripts.org/vim/scripts.html
Plugin 'L9'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" HTML Parser
" https://code.tutsplus.com/tutorials/vim-essential-plugin-sparkup--net-19281
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" http://daylerees.github.io/
Plugin 'daylerees/colour-schemes', {'rtp': 'vim/'}

" Vim에서 파일 탐색기를 사용할 수 있게 한다. - Nerd Tree
"Plugin 'The-NERD-tree'
Plugin 'scrooloose/nerdtree'

" Vim에서 자동완성 기능(Ctrl + P)을 키입력하지 않더라도 자동으로 나타나게 - AutoComplPop
Plugin 'AutoComplPop'          
Plugin 'shougo/neocomplete.vim'

" 열려있는 소스파일의 클래스, 함수, 변수 정보 창 - Tag List
Plugin 'taglist-plus'

" Airline Plugin (http://vimawesome.com/plugin/vim-airline-sad-beautiful-tragic)
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Node JS? Jade?"
Plugin 'node.js'

" Shell Script INDENT / SYNTAX Plugin
Plugin 'sh.vim'
Plugin 'bsh.vim'

" PHP Plugin (http://vimawesome.com/?q=tag:php)
Plugin 'StanAngeloff/php.vim'
Plugin '2072/PHP-Indenting-for-VIm'
Plugin 'shawncplus/phpcomplete.vim'

" Auto Syntax Checker (http://vimawesome.com/plugin/syntastic)
Plugin 'scrooloose/syntastic'

" for Markdown Preview
Plugin 'JamshedVesuna/vim-markdown-preview'

" for html automatic parse
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/gv.vim'
Plugin 'junegunn/limelight.vim'

" Auto Pair Plugin
Plugin 'jiangmiao/auto-pairs'

" Auto Commentary Plugin
Plugin 'scrooloose/nerdcommenter'

" Related to Javascript
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'SirVer/ultisnips'
Plugin 'isRuslan/vim-es6'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype on
filetype plugin indent on    " required : To ignore plugin indent changes, instead use ...

"--------------------------------------------------------------------------------------------------
" vim-jade plugin for Jade Syntax Highlighting
"--------------------------------------------------------------------------------------------------
au BufNewFile,BufReadPost *.jade set filetype=jade

"--------------------------------------------------------------------------------------------------
" Makdown Plugin Github Preview configured
"--------------------------------------------------------------------------------------------------
let vim_markdown_preview_browser='Firefox'
let vim_markdown_preview_github=1

"--------------------------------------------------------------------------------------------------
" NERD Tree를 왼쪽에 생성
"--------------------------------------------------------------------------------------------------
let NERDTreeWinPos = "left"

"--------------------------------------------------------------------------------------------------
" Tag list가 사용하는 ctags의 경로 설정
"--------------------------------------------------------------------------------------------------
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_Inc_Winwidth = 0
let Tlist_Exit_OnlyWindow = 0
let Tlist_Auto_Open = 0
let Tlist_Use_Right_Window = 1          " Tag list 창을 오른쪽으로 생성

"--------------------------------------------------------------------------------------------------
" Airline configured
"--------------------------------------------------------------------------------------------------
"
" Default
set laststatus=2
let g:airline_powerline_fonts = 1
"let g:airline_theme = 'molokai'
let g:airline_theme = 'powerlineish'
"let macvim_skip_colorscheme=1          " Issue : https://github.com/jacoborus/tender.vim/issues/9
"
" unicode symbols
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline_section_z = airline#section#create(['windowswap', '%3p%% ', 'linenr', ':%3v'])
let g:airline_left_sep = '»'
let g:airline_right_sep = '«'
let g:airline_symbols.linenr = 'Ln'
let g:airline_symbols.branch = 'ⓑ'
"let g:airline_symbols.paste = ''
let g:airline_symbols.whitespace = 'Ξ'
"
" Airline extension
let g:airline_extensions = ['branch', 'tabline']
let g:airline#extensions#tabline#enabled = 1                "버퍼 목록 켜기
"let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#fnamemod = ':t'           "파일명만 출력
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'

"--------------------------------------------------------------------------------------------------
" Syntastic 설정
"--------------------------------------------------------------------------------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1

" HTML 문법검사
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" set javascript syntax chacker for syntastic
"let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_checkers = ['jsxhint']

" AngularJS 지시자 ng-* 패턴 무시
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ng-"]

"--------------------------------------------------------------------------------------------------
" Theme 설정
"--------------------------------------------------------------------------------------------------
" apply molokai color scheme
let g:molokai_original = 1
let g:rehash256 = 1

"--------------------------------------------------------------------------------------------------
" ctrlp 설정
"--------------------------------------------------------------------------------------------------
let g:ctrlp_follow_symlinks=1
let g:ctrlp_clear_cache_on_exit=0


"let g:ctrlp_custom_ignore = 'tmp$\|\.git$\|\.hg$\|\.svn$\|.rvm$|.bundle$\|vendor'
let g:ctrlp_custom_ignore={
  \ 'dir':  '\.git$\|public$\|log$\|tmp$\|vendor$',
  \ 'file': '\v\.(exe|so|dll)$'
  \}

" Ignore these directories
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/vendor/*,*/\.git/*
"set wildignore+=*/out/**
"set wildignore+=*/vendor/**

" set no max file limit
"let g:ctrlp_max_files = 0
" Search from current directory instead of project root
"let g:ctrlp_working_path_mode = 0
let g:ctrlp_working_path_mode = 'ra'

let g:ctrlp_match_window_bottom=1
"let g:ctrlp_max_height=25
let g:ctrlp_match_window_reversed=0
let g:ctrlp_mruf_max=500

"--------------------------------------------------------------------------------------------------
" multiple Cursor
"--------------------------------------------------------------------------------------------------
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'
let g:multi_cursor_start_key='<C-n>'
let g:multi_cursor_start_word_key='g<C-n>'

"--------------------------------------------------------------------------------------------------
" Brief help
"--------------------------------------------------------------------------------------------------
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
"
"--------------------------------------------------------------------------------------------------
" End of File
"--------------------------------------------------------------------------------------------------
